# Exercise in Complex Numbers, C++ Oriented object 

Written for the "Software Design" course of M.EEC, in FEUP, Portugal

Main contributer:
- Armando Sousa    [email](mailto:asousa@fe.up.pt)


## License

[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html) - https://www.gnu.org/licenses/gpl-3.0.html

In short, free software: Useful, transparent, no warranty


## Description

A small exercise on syntax and features of OO programming in C.
Built using VSCode.
You must rebuild the *.json files.

This project features:
- Defining a class "Complex" with members real, imaginary and name (all public)
- Constructor and destructor print debugging messages
- The name for the Complex is optional, exemplifies using optional parameters and default name is ""
- Addition of complex numbers is based on a class member function class (uses "this->")
- Subtraction is operator overloaded (operator-)
- Insertion operator (operator<<) is also overloaded for easy printing (needs friend, etc)


**Attention:** This project does not intend to build complex numbers library... complex numbers are only a motivation

**Careful:** As mentioned in the sorce code, member variables should not be public!

## Support video(s)


## Tests
Tested in VSC 1.61, windows, g++ (Rev5, Built by MSYS2 project) 10.3.0

## Note
It is advocated to include "-Wall" in the compiler parameters




