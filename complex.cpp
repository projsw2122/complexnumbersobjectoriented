// Software Design M.EEC FEUP 
// Writtne mostly for Visual Studio Code, also mostly in C++ style (for debate)
// Author: Armando Sousa, FEUP, 2023 - asousa@fe.up.pt
// License:  GPL v3
// In short: Free software: Useful, transparent, absolutely no warranty at all, use at your own risk
// This program creates a complex number class, with a name, and some operations
// https://gitlab.com/feup_psw/complexnumbersobjectoriented

#include <iostream>   
#include <string>     
using namespace std;  


/// @brief Normal, printable Complex Numbers that might have a name = tag
class ComplexWithName {  
private:                                                   /// For better code quality, change to private:
    int re; // To store real part of complex number  
    int im; // To store imaginary part of complex number  
    std::string name;
public:  

    /// Constructor with defautl re=im=0 optional name default ""
    /// (Prints stuff for debug)
    ComplexWithName(string aName="")  
    {  
        re = 0;  
        im = 0; 
        name=aName;
        cout << "Debug: Constructor NoVals " << (*this) << endl;
    }  

    // Constructor, Re, Im, optional Name
    ComplexWithName(int r, int i, string aName="") // Constructor
    {  
        re = r; // r is initialized during object creation  
        im = i; // i is initialized during object creation  
        name=aName;
        cout << "Debug: Constructor W/Vals " << (*this) << endl;
    }  

    /// Destructor (just prints stuff for debug)
    /// (Prints stuff for debug)
    ~ComplexWithName() 
    {
        cout << "Debug: Destructor " << (*this) << endl;
    } 

    friend ostream& operator<<(ostream& os, const ComplexWithName& cpx);
    /// Member Function add (does not print anything)
    ComplexWithName addComplexNumber(ComplexWithName C, string name="")  
    {  
        ComplexWithName res; // result object of complex class  
        res.re = this->re + C.re;  
        res.im = this->im + C.im;  
        res.name = name;
        return res;  
    }  

    // Operator overloading (does not print anything)
     ComplexWithName operator - (const ComplexWithName &c2)
     {
          ComplexWithName res;
          res.re = re - c2.re;
          res.im = im - c2.im;
          res.name = "Diff: " + name + " - " + c2.name;
          return res;
     }

    // Operator overloading (does not print anything)
     ComplexWithName operator + (const ComplexWithName &c2)
     {
          ComplexWithName res;
          res.re = re + c2.re;
          res.im = im + c2.im;
          res.name = "Sum: " + name + " + " + c2.name;
          return res;
     }
};  // end class complex


# if 0
    // This exactly what you should not do, but it is here for comparison
    // add complex numbers without operator overloading
    ComplexWithName addComplexNumbers(ComplexWithName c1, ComplexWithName c2)
    {
        ComplexWithName res;
        res.re = c1.re + c2.re;
        res.im = c1.im + c2.im;
        return res;
    }
#endif


/// Friend of ComplexWithName, insertion operator
/// May be used to print
ostream& operator<<(ostream& os, const ComplexWithName& cpx)  
{
     if (cpx.name=="") {
         os << "[ "; 
     } else {
         os << cpx.name << "[ " ;
     } 
     os << cpx.re << " " << ", " << cpx.im << " i ]";
     return os;
}



/* 
/// Adds 3 complex numbers and returns sum
/// An "outsider" normal function, unrelated to the class?! ... 
/// This is exactly why public member variables are a bad idea!!!
ComplexWithName externalAdd3ComplexNumbers(const ComplexWithName A,const ComplexWithName B,const ComplexWithName C, string resultName)
{
    ComplexWithName res; // result object of complex class  
    res.re = A.re + B.re + C.re;  // member vars are public
    res.im = A.im + B.im + C.im;  // member vars are public
    res.name = resultName;        // member vars are public
    return res;  
}
// End of stuff regarding Complex class
*/


int main()  
{  
    cout << "Code Start" << endl;  

    ComplexWithName cpxNum1("cpxNum1");   
    ComplexWithName cpxNum2(2, 7, "cpxNum2");  

    {// Begin of forcing a temporary scope
        cout << "Inner Scope, cpxNum3 will be created" << endl;  
        // Sample Complex number, one that will not be used in future
        // ComplexWithName x=externalAdd3ComplexNumbers(cpxNum2,cpxNum2,cpxNum2,"Times3Cpx2"); /if members are public
        // ComplexWithName cpxNum3=cpxNum2.addComplexNumber(cpxNum2.addComplexNumber(cpxNum2));
        ComplexWithName cpxNum3("cpxNum3");
        cpxNum3 = cpxNum1 + cpxNum2 + cpxNum2 + cpxNum2;
        cout << "Result:" << cpxNum3 << endl;
        cout << "Inner Scope, cpxNum3 will be eliminated" << endl;  
    }// End of forcing a temporary scope
    
    cout << "Code will end now" << endl;
    
    return(0); 

    // printing (not using operator overloading and all members are public)
    //// cout << "Complex number 1 : " << cpxNum1.re << " + i" << cpxNum1.im << endl;  // Only possible because members are public
    //// cout << "Complex number 2 : " << cpxNum2.re << " + i" << cpxNum2.im << endl;  // Only possible because members are public
    cout << cpxNum1 << endl;  
    cout << cpxNum2 << endl;  
    cout << "- - - - - - - - - - - - -" << endl;  
    cout << "- - - - - - - - - - - - -" << endl;  

    // First Complex number  
    ComplexWithName A(2, 7, "A");  

    // printing first complex number  
    //// cout << "Complex number 1 : " << A.re << " + i" << A.im << endl;  // Only possible because members are public
    cout << A << endl;  
  
    // Second Complex number  
    ComplexWithName B(10, 6, "B");  
  
    // printing second complex number  
    //// cout << "Complex number 2 : " << B.re  << " + i" << B.im << endl;  // Only possible because members are public
    cout << B << endl;  
 
    // printing the sum and subtraction (calculated with different styles on purpose)
    ComplexWithName sum = A.addComplexNumber(B, "sum = A + B");  
    cout << "Sum      A+B : " << A << " + " << B << " = " << sum << endl;  
    cout << "Subtract A-B : " << A << " + " << B << " = " << A-B << endl;
    cout << "Subtract (1+2i)-(1-3i) : " << ComplexWithName(1,2) - ComplexWithName(1,-3) << endl;
}  





